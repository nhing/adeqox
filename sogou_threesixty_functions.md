# Sogou and Three Sixty Functions 

## sogou_retrieve_ideas (username,password,apitoken)

   input the username, password, apitoken associated with sogou account.
   in every index of the array is a hash.
   in each hash contains one or two keys.

   EVERY hash has the [:cpc_grp_id] key, which keeps track of the group_ids associated with every acct.
   
   SOME hashes have [:cpc_idea_types] key, which returns an array of the cpcs associated with each group id.
   
   an example of what returns when you pull it up:

   > ideas[0][:cpc_idea_types][0]
   > {:cpc_idea_id=>"1669633146", :cpc_grp_id=>"286867715", :title=>"{携程旅游} - 携程旅游 想游就有!", :description1=>"携程旅游度假覆盖全球300个旅游胜地,特有6重旅游保障.丰富,超值,可靠,高品质!", :description2=>"更有自由行,团队游,半自助,自驾游,邮轮等全系列旅游度假产品服务供您选择.", :visit_url=>"http://t.adeqo.com/click?company_id=1&network_id=77&campaign_id=186178759&adgroup_id=286867715&ad_id={creative}&keyword_id={keywordid}&cookie=30&device=pc&tv=v1&durl=http%3A%2F%2Fv.ctrip.com", :show_url=>"http://www.xiecheng.com", :pause=>false, :status=>"44", :opt=>{:opt_int=>{:key=>"tag", :value=>"0"}}}

   ideas[0][:cpc_idea_types] each contain 3 cpc_idea_types. so ideas[1][:cpc_idea_types] have 3, and so on.
   therefore it seems safe to say that IF a cpc_group (accessed by cpc_group_id) has cpc_idea_types, it will most likely
   have 3 cpc_idea_types associated with the cpc_group.

   every idea is composed of:
    [:cpc_idea_id, :cpc_grp_id, :title, :description1, :description2, :visit_url, :show_url, :pause, :status, :opt]
   seems like everythign after :show_url is not necessary to be shown.

## sogou_retrieve_cpcs (username,password,apitoken)
   sogou_retrieve_cpcs
   returns an array.
   in every index of the array is a hash.
   in each hash contains EITHER one or two keys.
   EVERY hash has the [:cpc_grp_id] key, which keeps track of the group_ids associated with every acct.
   SOME hashes have [:cpc_types] key, which returns an array of the cpcs associated with each group id.
   an example of what returns when you pull it up:

   > cpcs[0][:cpc_types][0]
   => {:cpc_id=>"3239384176", :cpc_grp_id=>"286867715", :cpc=>"携程旅游 马尔代夫", :price=>"3.0", :visit_url=>"http://u.ctrip.com/union/CtripRedirect.aspx?typeid=2&sid=725964&allianceid=4901&jumpurl=http://vacations.ctrip.com/", :match_type=>"0", :cpc_quality=>"3.0", :pause=>false, :status=>"37", :is_show=>"0", :opt=>{:opt_long=>{:key=>"useGroupPrice", :value=>"0"}, :opt_double=>{:key=>"CpcMobileQuality", :value=>"3.0"}}}

   a CPC is composed of the following:
   [:cpc_id, :cpc_grp_id, :cpc, :price, :visit_url, :match_type, :cpc_quality, :pause, :status, :is_show, :opt]
   everything after :cpc_quality doesn't seem like it needs to be shown.
