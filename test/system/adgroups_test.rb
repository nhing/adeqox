require "application_system_test_case"

class AdgroupsTest < ApplicationSystemTestCase
  setup do
    @adgroup = adgroups(:one)
  end

  test "visiting the index" do
    visit adgroups_url
    assert_selector "h1", text: "Adgroups"
  end

  test "creating a Adgroup" do
    visit adgroups_url
    click_on "New Adgroup"

    click_on "Create Adgroup"

    assert_text "Adgroup was successfully created"
    click_on "Back"
  end

  test "updating a Adgroup" do
    visit adgroups_url
    click_on "Edit", match: :first

    click_on "Update Adgroup"

    assert_text "Adgroup was successfully updated"
    click_on "Back"
  end

  test "destroying a Adgroup" do
    visit adgroups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Adgroup was successfully destroyed"
  end
end
