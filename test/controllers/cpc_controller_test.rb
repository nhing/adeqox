require 'test_helper'

class CpcControllerTest < ActionDispatch::IntegrationTest
  test "should get _cpc" do
    get cpc__cpc_url
    assert_response :success
  end

  test "should get index" do
    get cpc_index_url
    assert_response :success
  end

  test "should get edit" do
    get cpc_edit_url
    assert_response :success
  end

  test "should get show" do
    get cpc_show_url
    assert_response :success
  end

  test "should get create" do
    get cpc_create_url
    assert_response :success
  end

end
