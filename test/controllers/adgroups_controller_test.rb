require 'test_helper'

class AdgroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @adgroup = adgroups(:one)
  end

  test "should get index" do
    get adgroups_url
    assert_response :success
  end

  test "should get new" do
    get new_adgroup_url
    assert_response :success
  end

  test "should create adgroup" do
    assert_difference('Adgroup.count') do
      post adgroups_url, params: { adgroup: {  } }
    end

    assert_redirected_to adgroup_url(Adgroup.last)
  end

  test "should show adgroup" do
    get adgroup_url(@adgroup)
    assert_response :success
  end

  test "should get edit" do
    get edit_adgroup_url(@adgroup)
    assert_response :success
  end

  test "should update adgroup" do
    patch adgroup_url(@adgroup), params: { adgroup: {  } }
    assert_redirected_to adgroup_url(@adgroup)
  end

  test "should destroy adgroup" do
    assert_difference('Adgroup.count', -1) do
      delete adgroup_url(@adgroup)
    end

    assert_redirected_to adgroups_url
  end
end
