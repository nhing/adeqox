require 'test_helper'

class SogouIdeasControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get sogou_ideas_edit_url
    assert_response :success
  end

  test "should get index" do
    get sogou_ideas_index_url
    assert_response :success
  end

  test "should get new" do
    get sogou_ideas_new_url
    assert_response :success
  end

  test "should get show" do
    get sogou_ideas_show_url
    assert_response :success
  end

  test "should get _sogou_idea" do
    get sogou_ideas__sogou_idea_url
    assert_response :success
  end

end
