class Adgroup < ApplicationRecord
  belongs_to :campaign
  has_many :keywords, dependent: :destroy
  paginates_per 2
end
