module ApiCall
  class DataRetriever
    def initialize(reference_element:, username:, password:, apitoken:, apisecret:, target:)
      @reference_element = reference_element
      @username = username
      @password = password
      @apitoken = apitoken
      @apisecret = apisecret
      @target = target
    end

    def threesixty_login(username,password,apitoken,apisecret)
                  cipher_aes = OpenSSL::Cipher::AES.new(128, :CBC)
                  cipher_aes.encrypt
                  cipher_aes.key = apisecret[0,16]
                  cipher_aes.iv = apisecret[16,16]
                  encrypted = (cipher_aes.update(Digest::MD5.hexdigest(password)) + cipher_aes.final).unpack('H*').join
                  url = "https://api.e.360.cn/account/clientLogin"
                  response = HTTParty.post(url,
                  :timeout => 300,
                  :body => {
                          :username => username,
                          :passwd => encrypted[0,64]
                  },
                  :headers => {'apiKey' => apitoken }
                   )
                  return response.parsed_response
    end

    def threesixty_api( apitoken, access_token, service, method, params = {})
          url = "https://api.e.360.cn/2.0/#{service}/#{method}"
          response = HTTParty.post(url,
                                      timeout: 300,
                                      body: params,
                                  headers: {
                                          'apiKey' => apitoken,
                                           'accessToken' => access_token,
                                          'serveToken' => Time.now.to_i.to_s
                                  })
          @response = response
          if @response.headers["quotaremain"].to_i < 500
                  then logger.info "|||||Not Enough Quota||||"
                  return nil
          else
          return response.parsed_response
          end
    end

    def threesixty_campaignids(apitoken, access_token)
      all_campaigns = []
      campaign_array = []
      result = threesixty_api(apitoken.to_s,access_token,"account", "getCampaignIdList")
      if result.nil?
        return nil
      end
      campaign_array = result["account_getCampaignIdList_response"]["campaignIdList"]["item"]
      if campaign_array.is_a?(Hash)
        if !campaign_array.empty?
            all_campaigns << campaign_array
        end
      else
        all_campaigns = all_campaigns + campaign_array
      end
      if all_campaigns.count.to_i > 0
        return all_campaigns
      else
        return nil
        end
    end

    def threesixty_campaignname(apitoken, access_token, campaignid)
    campaign_name = ""
    body = {}
    body[:idList] = "["+campaignid.to_s+"]"
    result = threesixty_api( apitoken.to_s, access_token, "campaign", "getInfoByIdList", body)
      if result.nil?
        return nil
      end
      campaign_body= result["campaign_getInfoByIdList_response"]["campaignList"]["item"]
      campaign_name = campaign_body["name"]
      return campaign_name
    end

    def threesixty_adgroupid(apitoken, access_token,campaignid)
    adgroup_id_arr = []
    adgroup_id_arr_str = ""
    body = {}
    body[:campaignId] = campaignid
    result = threesixty_api( apitoken.to_s, access_token, "group", "getIdListByCampaignId", body)
      if result.nil?
       return nil
      end
      adgroups = result["group_getIdListByCampaignId_response"]["groupIdList"]["item"]
      if adgroups.is_a?(Array)
        if adgroups.count.to_i > 0
          adgroups.each do |adgroups_d|
            adgroup_id_arr << adgroups_d.to_i
          end
        end
      else
        adgroup_id_arr << adgroups.to_i
      end
      if adgroup_id_arr.count.to_i > 0
      return adgroup_id_arr
      else
      return nil
      end
    end

    def threesixty_adgroupname(apitoken, access_token, groupid)
    group_name = ""
    body = {}
    body[:idList] = "[" + groupid.to_s + "]"
    result = threesixty_api(apitoken.to_s, access_token, "group", "getInfoByIdList", body)
      if result.nil?
        return
        nil
      end
      group_body= result["group_getInfoByIdList_response"]["groupList"]["item"]
      group_name = group_body["name"]
      return group_name
    end

    def threesixty_keywordid(apitoken, access_token,adgroupid)
    body = {}
    body[:groupId] = adgroupid.to_i
    @result = threesixty_api( apitoken.to_s, access_token, "keyword", "getIdListByGroupId", body)
    if @result.nil?
       return nil
    end
    all_keyword = []
    keyword_id_arr = []
    keyword_id_arr_str = ""
      if @result["keyword_getIdListByGroupId_response"]["failures"].nil?
        if !@result["keyword_getIdListByGroupId_response"]["keywordIdList"].nil?
          if !@result["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"].nil?
            @keyword_id_status_body = @result["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"]
              if @keyword_id_status_body.is_a?(Array)
                if @keyword_id_status_body.count.to_i > 0
                @keyword_id_status_body.each do |keyword_id_status_body_d|
                keyword_id_arr << keyword_id_status_body_d.to_i
                  end
                end
              else
              keyword_id_arr << @keyword_id_status_body.to_i
              end
          end
        end
      end
      if keyword_id_arr.count.to_i > 0
        keyword_id_arr_str = keyword_id_arr.join(",")
        body = {}
        body[:idList] = "["+keyword_id_arr_str.to_s+"]"
        @result = threesixty_api( apitoken.to_s, access_token, "keyword", "getInfoByIdList", body)
        @kwarr= @result["keyword_getInfoByIdList_response"]["keywordList"]["item"]
        if @kwarr.is_a?(Hash)
          if !@kwarr.empty?
            all_keyword << @kwarr
          end
        else
          all_keyword = all_keyword + @kwarr
        end
      end
      if all_keyword.count.to_i > 0
      return all_keyword
      else
      return nil
      end
    end

    def call_api
    @response = threesixty_login(@username, @password, @apitoken, @apisecret)
    @accesstoken = @response["account_clientLogin_response"]["accessToken"]
      if @target == "campaigns"
        @retrieved_campaign_ids = threesixty_campaignids(@apitoken, @accesstoken)
        @account_campaigns= @reference_element.campaigns

        if @account_campaigns.count < 1
          @retrieved_campaign_ids.each do |id|
          @name = threesixty_campaignname(@apitoken, @accesstoken, id)
          @campaign= Campaign.create(:channel_id => id, :account_id => @reference_element.id, :name => @name)
          end
          @message = "Your #{@target} have been retrieved."
        else
          @account_campaign_ids = []
          @reference_element.campaigns.each do |c| @account_campaign_ids.push(c.channel_id)
          end
          if
            @retrieved_campaign_ids.sort == @account_campaign_ids.sort
            @message = "Your #{@target} are already up to date."
          else
          @account_campaign_ids.each do |id|
            unless id.in?@retrieved_campaign_ids
            Campaign.where(account_id: @reference_element.id).where(channel_id: id).first.destroy
              end
            end
          @retrieved_campaign_ids.each do |id|
            unless id.in?@account_campaign_ids
              @name = threesixty_campaignname(@apitoken, @accesstoken, id)
              Campaign.create(:channel_id => id, :account_id => @reference_element.id, :name => @name)
              end
            end
          @message = "Your #{@target} have been updated"
          end
        end

      elsif @target == "adgroups"

        @retrieved_adgroup_ids = threesixty_adgroupid(@apitoken, @accesstoken, @reference_element.channel_id)
        @campaign_adgroups = @reference_element.adgroups

        if @campaign_adgroups.count < 1
          @retrieved_adgroup_ids.each do |id|
          @name = threesixty_adgroupname(@apitoken, @accesstoken, id)
          @adgroup = Adgroup.create(:channel_id => id, :campaign_id => @reference_element.id, :name => @name)
          end
          @message = "Your adgroups have been retrieved."

        else
          @campaign_adgroup_ids = []
          @reference_element.adgroups.each do |a| @campaign_adgroup_ids.push(a.channel_id)
          end
          if @retrieved_adgroup_ids.sort == @campaign_adgroup_ids.sort
            @message = "Your adgroups are already up to date."
          else
          @campaign_adgroup_ids.each do |id|
            unless id.in?@retrieved_adgroup_ids
            Adgroup.where(campaign_id: @reference_element.id).where(channel_id: id).first.destroy
              end
            end
          @retrieved_adgroup_ids.each do |id|
            unless id.in?@campaign_adgroup_ids
              @name = threesixty_adgroupname(@apitoken, @accesstoken, id)
              Adgroup.create(:channel_id => id, :campaign_id => @reference_element.id, :name => @name)
              end
            end
          @message = "Your adgroups have been updated."
          end
        end

      elsif @target == "keywords"
        @retrieved_keywords = threesixty_keywordid(@apitoken, @accesstoken, @reference_element.channel_id)
        @adgroup_keyword_ids = []
        @reference_element.keywords.each do |c| @adgroup_keyword_ids.push(c.channel_id)
        end
        if @adgroup_keyword_ids.count < 1
          @retrieved_keywords.each do |k|
          @keyword = Keyword.create(:title => k["word"], :adgroup_id => @reference_element.id, :channel_id => k["id"])
          end
          @message = "Your keywords have been retrieved."

        else
          @retrieved_keywords_array = []
          @retrieved_keywords.each do |k| @retrieved_keywords_array.push(k["id"]) end
          if @retrieved_keywords_array.sort == @adgroup_keyword_ids.sort
            @message = "Your keywords are already up to date."
          else
            @adgroup_keyword_ids.each do |id|
            unless id.in?@retrieved_keywords_array
              Keyword.where(adgroup_id: @reference_element.id).where(channel_id: id).first.destroy
                end
              end
            @retrieved_keywords_array.each do |id|
            unless id.in?@adgroup_keyword_ids
             @keyword = @retrieved_keywords.find {|keyword| keyword["id"] == id}
              @title = @keyword["word"]
                Keyword.create(:channel_id => id, :adgroup_id => @reference_element.id, :title => @title)
              end
            end
          @message = "Your keywords have been updated."
          end
          end

        end
      end
    end
  end
