
#function to take a set of username, password and apitoken and output a list of URLs

def sogou_update_ad(username, password, apitoken)
  @username = username
  @password = password
	@apitoken = apitoken
	@group_id_array = []
	@ad_arr = []
	groupbegin = 0 # set 0 to start from the beginning
	totalchanges = 0
  sogou_api(@username, @password, @apitoken, "CpcGrpService")
  @update_status = @sogou_api.call(:get_all_cpc_grp_id)
	@update_status_body = @update_status.body.to_hash
	@result_active_grp = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]
	@result_active_grp.each do |el|
    if el[:cpc_grp_ids].is_a?(Array)
    el[:cpc_grp_ids].each do |id|
      @group_id_array << id
        end
    else
    @group_id_array << el[:cpc_grp_ids]
    end
    end
  sogou_api(@username, @password, @apitoken, "CpcIdeaService")
    @group_id_array[groupbegin..-1].each_with_index do |groupid, index|
    changes = 0
    @index = index + 1 + groupbegin
    @info = "[Sogou|AD|" + @username + "] [" + groupid.to_s + "][" + @index.to_s + "/" + @group_id_array.size.to_s + "] "
    logger.info @info + "Begin"
    @update_status = @sogou_api.call(:get_cpc_idea_by_cpc_grp_id, message: { cpcGrpIds: groupid, getTemp: 0 })
    @header = @update_status.header.to_hash
    @remain_quote = @header[:res_header][:rquota]
    @update_status_body = @update_status.body.to_hash
    @result_active_cpc = @update_status_body[:get_cpc_idea_by_cpc_grp_id_response][:cpc_grp_ideas]
    if @result_active_cpc[:cpc_idea_types].nil?
    then next
    end
    if !@result_active_cpc[:cpc_idea_types].nil?
    if @result_active_cpc[:cpc_idea_types].is_a?(Array)
    @ad_arr = @result_active_cpc[:cpc_idea_types]
    else
    @ad_arr << @result_active_cpc[:cpc_idea_types]
    end
    request_arr = []
    @ad_arr.each do |ad|
    request = {}
    if (ad[:visit_url]) && (ad[:visit_url].include? "adeqo")
    @new_url= URI.unescape(ad[:visit_url].scan(/=(http[^>]*)/).last.first)
    request[:cpcIdeaId] = ad[:cpc_idea_id]
    request[:visitUrl] = @new_url
    end
    if (ad[:mobile_visit_url]) && (ad[:mobile_visit_url].include? "adeqo")
    @new_url= URI.unescape(ad[:mobile_visit_url].scan(/=(http[^>]*)/).last.first)
    request[:cpcIdeaId] = ad[:cpc_idea_id]
    request[:mobileVisitUrl] = @new_url
      end
    if request.size.to_i == 0 then next end
    request_arr << request
    changes = changes + 1
    rescue Timeout::Error
    logger.info "Timeout... waiting 30s then trying again"
    sleep(30)
    redo
    end
    end
  if request_arr.size.to_i == 0 then next end
  request_arr.each_slice(500) do |sub_arr|
  @result = @sogou_api.call(:update_cpc_idea, message: {cpcIdeaTypes: sub_arr})
  @header = @result.header.to_hash
    @update_response = @header[:res_header][:desc]
    if @update_response.to_s.downcase == "success"
    changes = changes + sub_arr.size
    end
    end
    if changes > 0
   totalchanges = totalchanges + changes
   logger.info @info + "Changes [" + changes.to_s + "] End"
    end
    end
   logger.info @info + "Total Changes [" + totalchanges.to_s + "] End"
end

def sogou_update_kw(username, password, apitoken)
  @username = username
  @password = password
  @apitoken = apitoken
  @group_id_array = []
  @kw_arr = []
  groupbegin = 0
  totalchanges = 0
  sogou_api(@username, @password, @apitoken, "CpcGrpService")
  @update_status = @sogou_api.call(:get_all_cpc_grp_id)
  @update_status_body = @update_status.body.to_hash
  @result_active_grp = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]
  @result_active_grp.each do |el|
    if el[:cpc_grp_ids].is_a?(Array)
    el[:cpc_grp_ids].each do |id| @group_id_array << id
    end
    else
    @group_id_array << el[:cpc_grp_ids]
    end
    end
    sogou_api(@username, @password, @apitoken, "CpcService")
    @group_id_array[groupbegin..-1].each_with_index do |groupid, index|
    changes = 0
    @index = index + 1 + groupbegin
    @info = "[Sogou|KW|" + @username + "] [" + groupid.to_s + "][" + @index.to_s + "/" + @group_id_array.size.to_s + "] "
    logger.info @info + "Begin"
    @update_status = @sogou_api.call(:get_cpc_by_cpc_grp_id, message: { cpcGrpIds: groupid, getTemp: 0 })
    @header = @update_status.header.to_hash
    @remain_quote = @header[:res_header][:rquota]
    @update_status_body = @update_status.body.to_hash
    @result_active_cpc = @update_status_body[:get_cpc_by_cpc_grp_id_response][:cpc_grp_cpcs]
    if @result_active_cpc[:cpc_types].nil?
    then next
    end
    if !@result_active_cpc[:cpc_types].nil?
      if !@result_active_cpc[:cpc_types].is_a?(Array)
      @kw_arr << @result_active_cpc[:cpc_types]
      else
      @kw_arr = @result_active_cpc[:cpc_types]
      end
      request_arr = []
        @kw_arr.each do |kw|
        request = {}
        if (kw[:visit_url]) && (kw[:visit_url].include? "adeqo")
          @new_url= URI.unescape(kw[:visit_url].scan(/=(http[^>]*)/).last.first)
          request[:cpcId] = kw[:cpc_id]
          request[:visitUrl] = @new_url
          end
        if (kw[:mobile_visit_url]) && (kw[:mobile_visit_url].include? "adeqo")
          @new_url= URI.unescape(kw[:mobile_visit_url].scan(/=(http[^>]*)/).last.first)
          request[:cpcId] = kw[:cpc_id]
          request[:mobileVisitUrl] = @new_url
        end
        if request.size.to_i == 0
          then next
        end
        request_arr << request
        changes = changes + 1
        rescue Timeout::Error
        logger.info "Timeout... waiting 30s then trying again"
        sleep(30)
        redo
      end
    end
    if request_arr.size.to_i == 0 then
      next
    end
    request_arr.each_slice(500) do |sub_arr| # set a number less than max number of API
      @result = @sogou_api.call(:update_cpc, message: {cpcTypes: sub_arr})
      @header = @result.header.to_hash
      @update_response = @header[:res_header][:desc]
      if @update_response.to_s.downcase == "success"
        changes = changes + sub_arr.size
      end
    end
    if changes > 0
      totalchanges = totalchanges + changes
      logger.info @info + "Changes [" + changes.to_s + "] End"
    end
  end
 logger.info @info + "Total Changes [" + totalchanges.to_s + "] End"
end

def sogou_login(username, password, token, api_string)
 @sogou_api = Savon.client(
 wsdl: "http://api.agent.sogou.com:80/sem/sms/v1/" + api_string + "?wsdl",
 pretty_print_xml: true,
 log: true,
 env_namespace: :soap,
 namespaces: {"xmlns:common" => "http://api.sogou.com/sem/common/v1"},
 soap_header: {
  "common:AuthHeader" => {
  'common:token' => token,
  'common:username' => username,
	'common:password' => password
 }
 }
)
end



def sogou_api(username, password, token, api_string)
 @sogou_api = Savon.client(
  wsdl: "http://api.agent.sogou.com:80/sem/sms/v1/" + api_string + "?wsdl",
	pretty_print_xml: true,
	log: true,
	env_namespace: :soap,
	namespaces: {"xmlns:common" => "http://api.sogou.com/sem/common/v1"},
	soap_header: {
	 "common:AuthHeader" => {
   'common:token' => token,
   'common:username' => username,
   'common:password' => password
  }
	}
)
end

def sogou_groupid(username, password, apitoken)
 group_id_array = []
 group_id_array_string = ""
 result = sogou_api(username, password, apitoken, "CpcGrpService")
   if result.nil?
    return nil
   end
  update_status = sogou_api.call(:get_all_cpc_grp_id)
	update_status_body = update_status.body.to_hash
	adgroups = update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]
	adgroups.each do |el|
    if el[:cpc_grp_ids].is_a?(Array)
     el[:cpc_grp_ids].each do |id| group_id_array << id
   end
   else
   group_id_array << el[:cpc_grp_ids]
   end
  end
  if group_id_array.count.to_i > 0
   return group_id_array
  else
   return nil
  end
end
