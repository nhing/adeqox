json.extract! adgroup, :id, :created_at, :updated_at
json.url adgroup_url(adgroup, format: :json)
