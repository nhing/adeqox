class SogouGroupsController < ApplicationController
  before_action :find_sogou_group, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :owned_sogou_group, only: [:show, :edit, :update, :destroy]
  def new
    @sogou_group = SogouGroup.new
  end

  def create
    @sogou_group = SogouGroup.new(sogou_group_params)
  end

  def sogou_retrieve_cpcs

    @group = SogouGroup.find(params[:id])
    @account = @group.sogou_plan.account
    @username = @account.username
    @password = @account.password
    @apitoken = @account.apitoken

    @group_id_array = sogou_retrieve_group_ids(@username, @password, @apitoken)


    @user = sogou_api(@username,@password,@apitoken,"CpcService")

    # retrieve response to API call, convert to hash

    @update_status = @user.call(:get_cpc_id_by_cpc_grp_id, message: {cpcGrpIds: @group_id_array, getTemp: 0})
    
    @stathash = @update_status.body.to_hash

    # retrieve the ideas from the hash

    @cpcs = @stathash[:get_cpc_id_by_cpc_grp_id_response][:cpc_grp_cpc_ids]

    @cpcs.each do |cpc|
      
      if cpc.keys.count >= 2
        
        @status = @user.call(:get_cpc_by_cpc_id, message: { cpcIds: cpc[:cpc_ids] })
        cpc_to_s=@status.body.to_hash
        
        cpc_to_s = cpc_to_s[:get_cpc_by_cpc_id_response]
        cpc_to_s.delete(:"@xmlns:ns2")
        cpc_to_s.delete(:"@xmlns:ns3")

        
            #         @update_status = @user.call(:get_cpc_id_by_cpc_grp_id, message: {cpcGrpIds: @group_id_array, getTemp: 0})
    # cpc_id = @update_status.body.to_hash
    # cpc_id = cpc_id[:get_cpc_id_by_cpc_grp_id_response][:cpc_grp_cpc_ids]

    # cpcs = {}

    # cpc_id.each do |cpc|
    #   if cpc.keys.count >= 2
    #     @status = @user.call(:get_cpc_by_cpc_id, message: { cpcIds: cpc[:cpc_ids] })
    #     cpcs[cpc[:cpc_grp_id]]=@status.body.to_hash
    #   end
    # end


      for j in 0..cpc_to_s[:cpc_types].length
        cpc_tos = cpc_to_s[:cpc_types][j]
        byebug
        if cpc_tos.has_key?(:mobile_visit_url)
          cpc_tos.delete(:mobile_visit_url)
        end

        cpc_to_su = Cpc.new(cpc_tos)
        @group.cpcs.push(cpc_to_su)

      end
      end
    end

    # @cpcs.each do |cpc|
    #   @idea = idea
    #   group_id = @idea[:cpc_grp_id]
    #   if group_id = @group.cpc_grp_id
    #     for i in 0..@idea[:cpc_idea_types].length
    #       ideax = @idea[:cpc_idea_types][i]
    #       sgi = SogouIdea.create(ideax)
    #       @group.sogou_ideas.push(sgi)
    #   end
    #   end 
    # end
    @account.save
    redirect_to @group
    flash[:success] = "CPCs for #{@account.channel}-#{@account.username} retrieved."


  end

  def sogou_retrieve_ideas

  	@group = SogouGroup.find(params[:id])
    @account = @group.sogou_plan.account
    @username = @account.username
    @password = @account.password
    @apitoken = @account.apitoken
    @group_id_array = sogou_retrieve_group_ids(@username, @password, @apitoken)


    @user = sogou_api(@username,@password,@apitoken,"CpcIdeaService")

    # retrieve response to API call, convert to hash

    @update_status = @user.call(:get_cpc_idea_by_cpc_grp_id, message: { cpcGrpIds: @group_id_array })
    
    @stathash = @update_status.body.to_hash

    # retrieve the ideas from the hash

    @ideas = @stathash[:get_cpc_idea_by_cpc_grp_id_response][:cpc_grp_ideas]
	  @ideas.each do |idea|
      @idea = idea
      group_id = @idea[:cpc_grp_id]
      if group_id = @group.cpc_grp_id
      	for i in 0..@idea[:cpc_idea_types].length
      		ideax = @idea[:cpc_idea_types][i]
      		sgi = SogouIdea.create(ideax)
      		@group.sogou_ideas.push(sgi)
  		end
  	  end 
    end
    @account.save
    redirect_to @group
    flash[:success] = "Ideas for #{@account.channel}-#{@account.username} retrieved."

  end

  def sogou_delete_ideas
    # operates under the assumption that user has already created an account.
    @sogou_group = SogouGroup.find(params[:id])

    if @sogou_group.sogou_ideas.count > 0
      @sogou_group.sogou_ideas.destroy_all
      flash[:success] = "Your ideas have been deleted."
    else
      flash[:success] = "No ideas to delete."
    end
    redirect_to @sogou_group
  end

  def sogou_delete_cpcs
    # operates under the assumption that user has already created an account.
    @sogou_group = SogouGroup.find(params[:id])

    if @sogou_group.cpcs.count > 0
      @sogou_group.cpcs.destroy_all
      flash[:success] = "Your cpcs have been deleted."
    else
      flash[:success] = "No cpcs to delete."
    end
    redirect_to @sogou_group
  end
   def destroy
    session[:return_to] ||= request.referer
    @sogou_group.destroy
    respond_to do |format|
      format.html { redirect_to session.delete(:return_to), notice: 'Plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private 
  	def find_sogou_group
      @sogou_group = SogouGroup.find(params[:id])
    end

    def owned_sogou_group
      unless current_user.id == @sogou_group.sogou_plan.account.user_id
      redirect_to root_path
      end
    end
     def sogou_group_params
      params.require(:sogou_group).permit(:cpc_grp_id, :cpc_plan_id, :cpc_grp_name, 
      	:max_price,:negative_words, :exact_negative_words,:pause, :status, :opt)
    end

end