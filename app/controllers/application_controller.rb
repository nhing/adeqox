class ApplicationController < ActionController::Base
  def after_sign_in_path_for(resource)
    accounts_path
  end

  #sogou api call using Savon

  def sogou_api(username, password, token, api_string)
    @sogou_api = Savon.client(
      wsdl: "http://api.agent.sogou.com:80/sem/sms/v1/" + api_string + "?wsdl",
      basic_auth: [ 'username', 'password' ],
      pretty_print_xml: true,
      log: true,
      env_namespace: :soap,
      namespaces: {"xmlns:common" => "http://api.sogou.com/sem/common/v1"},
      soap_header: {
        "common:AuthHeader" => {
          'common:token' => token,
          'common:username' => username,
          'common:password' => password
        }
      }
    )
  end

  def sogou_login(username,password,apitoken)
    @username = username
    @password = password
    @apitoken = apitoken

    @user = sogou_api(@username,@password,@apitoken,"AccountService")
    @user = @user.call(:get_account_info)
    @user = @user.header

    return @user

  end


  def sogou_retrieve_plan_ids(username,password,apitoken)
    @username = username
    @password = password
    @apitoken = apitoken

    sogou_api(@username, @password, @apitoken, "CpcPlanService")

    @update_status = @sogou_api.call(:get_all_cpc_plan_id)

    @plan_ids = @update_status.body.to_hash[:get_all_cpc_plan_id_response]

    return @plan_ids

  end



# retrieves group ids related to sogou.
# details: returns an array of group ids associated with the account.

  def sogou_retrieve_group_ids(username,password,apitoken)
    @username = username
    @password = password
    @apitoken = apitoken
    @uinfo = "[Sogou|AD|" + @username + "] "

    sogou_api(@username, @password, @apitoken, "CpcGrpService")
    @update_status = @sogou_api.call(:get_all_cpc_grp_id)

    @header = @update_status.header.to_hash
    @msg = @header[:res_header][:desc]
    @remain_quota = @header[:res_header][:rquota]
    logger.info @uinfo + @header.to_s
    if @msg.to_s.downcase == "failure"
      logger.info @uinfo + @header[:res_header][:failures][:message]
      return
    else
      @update_status_body = @update_status.body

      @raw_groups = @update_status_body
      @raw_groups = @raw_groups.to_hash

      result_active_grp = @raw_groups[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]

      # result_active_grp retrieves an array composed of hashes with two keys: one which
      # corresponds to a String indicating the plan id, and another which corresponds
      # to an array of group IDs, each of them are a StringWithAttributes.

      @group_id_array=[]

      # the following loop goes through everything in the result_active_grp and adds all elements
      # in the [:cpc_grp_ids].

      result_active_grp.each do |el|
            if el[:cpc_grp_ids].is_a?(Array)
              el[:cpc_grp_ids].each do |id| @group_id_array << id end
            else
              @group_id_array << el[:cpc_grp_ids]
            end
          end

      return @group_id_array

      # the below commands allow us to retrieve more specific information.
      # ignore unless you need to use it for some reason

      # @update_status_body = @update_status.body.to_hash
      # @cpc_plan_id = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids][0][:cpc_plan_id]
      # @cpc_grp_ids = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids][0][:cpc_grp_ids]
      # return @cpc_plan_id, @cpc_grp_ids

    end

  end

  def sogou_retrieve_groups(username,password,apitoken)
    @username = username
    @password = password
    @apitoken = apitoken

    @group_id_array = sogou_retrieve_group_ids(@username, @password, @apitoken)

    @user = sogou_api(@username,@password,@apitoken,"CpcGrpService")

    @update_status = @user.call(:get_cpc_grp_by_cpc_grp_id, message: {cpcGrpIds: @group_id_array})

    @update_status = @update_status.body[:get_cpc_grp_by_cpc_grp_id_response][:cpc_grp_types]
    
    return @update_status

  end

  # input the username, password, apitoken associated with sogou account.
  # in every index of the array is a hash.
  # in each hash contains one or two keys.

  # EVERY hash has the [:cpc_grp_id] key, which keeps track of the group_ids associated with every acct.
  # SOME hashes have [:cpc_idea_types] key, which returns an array of the cpcs associated with each group id.
  # an example of what returns when you pull it up:

  # > ideas[0][:cpc_idea_types][0]
  # {:cpc_idea_id=>"1669633146", :cpc_grp_id=>"286867715", :title=>"{携程旅游} - 携程旅游 想游就有!", :description1=>"携程旅游度假覆盖全球300个旅游胜地,特有6重旅游保障.丰富,超值,可靠,高品质!", :description2=>"更有自由行,团队游,半自助,自驾游,邮轮等全系列旅游度假产品服务供您选择.", :visit_url=>"http://t.adeqo.com/click?company_id=1&network_id=77&campaign_id=186178759&adgroup_id=286867715&ad_id={creative}&keyword_id={keywordid}&cookie=30&device=pc&tv=v1&durl=http%3A%2F%2Fv.ctrip.com", :show_url=>"http://www.xiecheng.com", :pause=>false, :status=>"44", :opt=>{:opt_int=>{:key=>"tag", :value=>"0"}}}

  # ideas[0][:cpc_idea_types] each contain 3 cpc_idea_types. so ideas[1][:cpc_idea_types] have 3, and so on.
  # therefore it seems safe to say that IF a cpc_group (accessed by cpc_group_id) has cpc_idea_types, it will most likely
  # have 3 cpc_idea_types associated with the cpc_group.

  # every idea is composed of:
  #  [:cpc_idea_id, :cpc_grp_id, :title, :description1, :description2, :visit_url, :show_url, :pause, :status, :opt]
  # seems like everythign after :show_url is not necessary to be shown.


  def sogou_retrieve_ideas(username, password, apitoken)
    @username = username
    @password = password
    @apitoken = apitoken

    @group_id_array = sogou_retrieve_group_ids(@username, @password, @apitoken)

    @user = sogou_api(@username,@password,@apitoken,"CpcIdeaService")

    # retrieve response to API call, convert to hash

    @update_status = @user.call(:get_cpc_idea_by_cpc_grp_id, message: { cpcGrpIds: @group_id_array, getTemp: 0 })
    @stathash = @update_status.body.to_hash

    # retrieve the ideas from the hash

    @ideas = @stathash[:get_cpc_idea_by_cpc_grp_id_response][:cpc_grp_ideas]
    return @ideas

  end

  # sogou_retrieve_cpcs
  # returns an array.
  # in every index of the array is a hash.
  # in each hash contains EITHER one or two keys.
  # EVERY hash has the [:cpc_grp_id] key, which keeps track of the group_ids associated with every acct.
  # SOME hashes have [:cpc_types] key, which returns an array of the cpcs associated with each group id.
  # an example of what returns when you pull it up:

  # > cpcs[0][:cpc_types][0]
  # => {:cpc_id=>"3239384176", :cpc_grp_id=>"286867715", :cpc=>"携程旅游 马尔代夫", :price=>"3.0", :visit_url=>"http://u.ctrip.com/union/CtripRedirect.aspx?typeid=2&sid=725964&allianceid=4901&jumpurl=http://vacations.ctrip.com/", :match_type=>"0", :cpc_quality=>"3.0", :pause=>false, :status=>"37", :is_show=>"0", :opt=>{:opt_long=>{:key=>"useGroupPrice", :value=>"0"}, :opt_double=>{:key=>"CpcMobileQuality", :value=>"3.0"}}}

  # a CPC is composed of the following:
  # [:cpc_id, :cpc_grp_id, :cpc, :price, :visit_url, :match_type, :cpc_quality, :pause, :status, :is_show, :opt]
  # everything after :cpc_quality doesn't seem like it needs to be shown.

  def sogou_retrieve_cpcs(username, password, apitoken)
    @username = username
    @password = password
    @apitoken = apitoken

    @group_id_array = sogou_retrieve_group_ids(@username, @password, @apitoken)

    @user = sogou_api(@username,@password,@apitoken,"CpcService")

    # @update_status = @user.call(:get_cpc_by_cpc_grp_id, message: { cpcGrpIds: @group_id_array, getTemp: 0 })
    # @stathash = @update_status.body.to_hash

    # @cpcs = @stathash[:get_cpc_by_cpc_grp_id_response][:cpc_grp_cpcs]

    @update_status = @user.call(:get_cpc_id_by_cpc_grp_id, message: {cpcGrpIds: @group_id_array, getTemp: 0})
    cpc_id = @update_status.body.to_hash
    cpc_id = cpc_id[:get_cpc_id_by_cpc_grp_id_response][:cpc_grp_cpc_ids]

    cpcs = {}

    cpc_id.each do |cpc|
      if cpc.keys.count >= 2
        @status = @user.call(:get_cpc_by_cpc_id, message: { cpcIds: cpc[:cpc_ids] })
        cpcs[cpc[:cpc_grp_id]]=@status.body.to_hash
      end
    end

    return cpcs

  end


#function to take a set of username, password and apitoken and output a list of URLs

  def sogou_update_ad(username, password, apitoken)

    @username = username
    @password = password
    @apitoken = apitoken
    @group_id_array = []
    @ad_arr = []
    @uinfo = "[Sogou|AD|" + @username + "] "
    groupbegin = 0 # set 0 to start from the beginning
    totalchanges = 0
    sogou_api(@username, @password, @apitoken, "CpcGrpService")
    @update_status = @sogou_api.call(:get_all_cpc_grp_id)
    @header = @update_status.header.to_hash
    @msg = @header[:res_header][:desc]
    @remain_quota = @header[:res_header][:rquota]
    #logger.info @uinfo + @header.to_s
    if @msg.to_s.downcase == "failure"
      logger.info @uinfo + @header[:res_header][:failures][:message]
      return
    else
      logger.info @uinfo + @msg
    end
    if !@remain_quota.nil?
      logger.info @uinfo + "Quota [" + @remain_quota.to_s + "]"
      return
    else
      logger.info @uinfo + "No Quota"
      return
    end
    @update_status_body = @update_status.body.to_hash
    @result_active_grp = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]
    @result_active_grp.each do |el|
      if el[:cpc_grp_ids].is_a?(Array)
        el[:cpc_grp_ids].each do |id| @group_id_array << id end
      else
        @group_id_array << el[:cpc_grp_ids]
      end
    end
    sogou_api(@username, @password, @apitoken, "CpcIdeaService")
    #logger.info @group_id_array.size #22144
    #@group_id_array = ["238876896", "238895734", "215628567"] #index 20106, 20107(nil), 20108
    @group_id_array[groupbegin..-1].each_with_index do |groupid, index|
      changes = 0
      @index = index + 1 + groupbegin
      @ginfo = "[" + groupid.to_s + "][" + @index.to_s + "/" + @group_id_array.size.to_s + "] "
      @info = @uinfo + @ginfo
      logger.info @info + "Begin"
      @update_status = @sogou_api.call(:get_cpc_idea_by_cpc_grp_id, message: { cpcGrpIds: groupid, getTemp: 0 })
      @header = @update_status.header.to_hash
      #logger.info @msg = @header[:res_header][:desc]
      @remain_quote = @header[:res_header][:rquota]
      @update_status_body = @update_status.body.to_hash
      @result_active_cpc = @update_status_body[:get_cpc_idea_by_cpc_grp_id_response][:cpc_grp_ideas]
      #logger.info "[Sogou][AD] " + @result_active_cpc.to_s
      if @result_active_cpc[:cpc_idea_types].nil? then next end
      if !@result_active_cpc[:cpc_idea_types].nil?
        #logger.info "[Sogou] CPC Size [" + @result_active_cpc[:cpc_idea_types].size.to_s + "]"
        if @result_active_cpc[:cpc_idea_types].is_a?(Array)
          @ad_arr = @result_active_cpc[:cpc_idea_types]
        else
          @ad_arr << @result_active_cpc[:cpc_idea_types]
        end
        #logger.info @info + "All AD Size: [" + @ad_arr.size.to_s + "] All AD: " + @ad_arr.to_s
        request_arr = []
        @ad_arr.each do |ad|
          #logger.info @info + "AD: " + ad.to_s
          request = {}
          if (ad[:visit_url]) && (ad[:visit_url].include? "adeqo") && (ad[:visit_url].include? "ctrip")
            @new_url= URI.unescape(ad[:visit_url].scan(/=(http[^>]*)/).last.first)
            request[:cpcIdeaId] = ad[:cpc_idea_id]
            request[:visitUrl] = @new_url
          end
          if (ad[:mobile_visit_url]) && (ad[:mobile_visit_url].include? "adeqo") && (ad[:mobile_visit_url].include? "ctrip")
            @new_url= URI.unescape(ad[:mobile_visit_url].scan(/=(http[^>]*)/).last.first)
            request[:cpcIdeaId] = ad[:cpc_idea_id]
            request[:mobileVisitUrl] = @new_url
          end
          if request.size.to_i == 0 then next end
          request_arr << request
          changes = changes + 1
          rescue Timeout::Error
            logger.info "Timeout... waiting 30s then trying again"
            sleep(30)
          redo
        end
      end
      #logger.info @info + "Request Size: [" + request_arr.size.to_s + "] Request: " + request_arr.to_s
      if request_arr.size.to_i == 0 then next end
      request_arr.each_slice(500) do |sub_arr| # set a number less than max number of API
        @result = @sogou_api.call(:update_cpc_idea, message: {cpcIdeaTypes: sub_arr})
        @header = @result.header.to_hash
        @update_response = @header[:res_header][:desc]
        if @update_response.to_s.downcase == "success"
          changes = changes + sub_arr.size
        end
      end
      if changes > 0
        totalchanges = totalchanges + changes
        logger.info @info + "Changes [" + changes.to_s + "] End"
      end
      #return
    end

    logger.info @info + "Total Changes [" + totalchanges.to_s + "] End"
    return @update_status_body
    #return @update_status_body

  end

  #function to take a set of username, password and apitoken and output a list of URLs

  def sogou_update_kw(username, password, apitoken)

    @username = username
    @password = password
    @apitoken = apitoken
    @group_id_array = []
    @kw_arr = []
    @uinfo = "[Sogou|KW|" + @username + "] "
    groupbegin = 0 # set 0 to start from the beginning
    totalchanges = 0

    #@group_id_array = sogou_groupid(@username, @password, @apitoken) #not work yet, to fix
    #logger.info @group_id_array.size
    #return

          sogou_api(@username, @password, @apitoken, "CpcGrpService")
          @update_status = @sogou_api.call(:get_all_cpc_grp_id)
    @header = @update_status.header.to_hash
    @msg = @header[:res_header][:desc]
    @remain_quota = @header[:res_header][:rquota]
    #logger.info @uinfo + @header.to_s
    if @msg.to_s.downcase == "failure"
      logger.info @uinfo + @header[:res_header][:failures][:message]
      return
    else
      logger.info @uinfo + @msg
    end
    if !@remain_quota.nil?
      logger.info @uinfo + "Quota [" + @remain_quota.to_s + "]"
      return
    else
      logger.info @uinfo + "No Quota"
      return
    end
    @update_status_body = @update_status.body.to_hash
    @result_active_grp = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]
    @result_active_grp.each do |el|
      if el[:cpc_grp_ids].is_a?(Array)
        el[:cpc_grp_ids].each do |id| @group_id_array << id end
      else
        @group_id_array << el[:cpc_grp_ids]
      end
    end
    sogou_api(@username, @password, @apitoken, "CpcService")
    #logger.info @group_id_array.size #22144
    #@group_id_array = ["238876896", "238895734", "215628567"] #index 20106, 20107(nil), 20108
    @group_id_array[groupbegin..-1].each_with_index do |groupid, index|
      changes = 0
      @index = index + 1 + groupbegin
      @ginfo = "[" + groupid.to_s + "][" + @index.to_s + "/" + @group_id_array.size.to_s + "] "
      @info = @uinfo + @ginfo
      logger.info @info + "Begin"
      @update_status = @sogou_api.call(:get_cpc_by_cpc_grp_id, message: { cpcGrpIds: groupid, getTemp: 0 })
      @header = @update_status.header.to_hash
      #logger.info @msg = @header[:res_header][:desc]
      @remain_quote = @header[:res_header][:rquota]
      @update_status_body = @update_status.body.to_hash
      @result_active_cpc = @update_status_body[:get_cpc_by_cpc_grp_id_response][:cpc_grp_cpcs]
      #logger.info "[Sogou][KW] " + @result_active_cpc.to_s
      if @result_active_cpc[:cpc_types].nil? then next end
      if !@result_active_cpc[:cpc_types].nil?
        #logger.info "[Sogou] CPC Size [" + @result_active_cpc[:cpc_types].size.to_s + "]"
        if !@result_active_cpc[:cpc_types].is_a?(Array)
          @kw_arr << @result_active_cpc[:cpc_types]
        else
          @kw_arr = @result_active_cpc[:cpc_types]
        end
        #logger.info @info + "All KW Size: [" + @kw_arr.size.to_s + "] All KW: " + @kw_arr.to_s
        #return
        request_arr = []
        @kw_arr.each do |kw|
          #logger.info @info + "KW: " + kw.to_s
          request = {}
          if (kw[:visit_url]) && (kw[:visit_url].include? "adeqo") && (kw[:visit_url].include? "ctrip")
            @new_url= URI.unescape(kw[:visit_url].scan(/=(http[^>]*)/).last.first)
            request[:cpcId] = kw[:cpc_id]
            request[:visitUrl] = @new_url
          end
          if (kw[:mobile_visit_url]) && (kw[:mobile_visit_url].include? "adeqo") && (kw[:mobile_visit_url].include? "ctrip")
            @new_url= URI.unescape(kw[:mobile_visit_url].scan(/=(http[^>]*)/).last.first)
            request[:cpcId] = kw[:cpc_id]
            request[:mobileVisitUrl] = @new_url
          end
          if request.size.to_i == 0 then next end
          request_arr << request
          changes = changes + 1
          rescue Timeout::Error
            logger.info "Timeout... waiting 30s then trying again"
            sleep(30)
          redo
        end
      end
      #logger.info @info + "Request Size: [" + request_arr.size.to_s + "] Request: " + request_arr.to_s
      if request_arr.size.to_i == 0 then next end
      request_arr.each_slice(500) do |sub_arr| # set a number less than max number of API
        @result = @sogou_api.call(:update_cpc, message: {cpcTypes: sub_arr})
        @header = @result.header.to_hash
        @update_response = @header[:res_header][:desc]
        if @update_response.to_s.downcase == "success"
          changes = changes + sub_arr.size
        end
      end
      if changes > 0
        totalchanges = totalchanges + changes
        logger.info @info + "Changes [" + changes.to_s + "] End"
      end
      #return
    end
    logger.info @info + "Total Changes [" + totalchanges.to_s + "] End"
    #return

    begin
      rescue
      logger.info "-----Error Class-----"
      logger.info $!.class
      logger.info $@.class
      logger.info "-----Error Message-----"
      logger.info $!
      logger.info $@[0]

      @error = []
      @error << $i.class << $i << $@.class << $@[0]
      render plain: @error
      #render plain: "hello"
      #render :json => @error
      #render :action => "error", :error => @error #not work yet
    end
  end


  def threesixty_login(username,password,apitoken,apisecret)
                cipher_aes = OpenSSL::Cipher::AES.new(128, :CBC)
                cipher_aes.encrypt
                cipher_aes.key = apisecret[0,16]
                cipher_aes.iv = apisecret[16,16]
                encrypted = (cipher_aes.update(Digest::MD5.hexdigest(password)) + cipher_aes.final).unpack('H*').join
                url = "https://api.e.360.cn/account/clientLogin"
                response = HTTParty.post(url,
                :timeout => 300,
                :body => {
                        :username => username,
                        :passwd => encrypted[0,64]
                },
                :headers => {'apiKey' => apitoken }
                 )
                return response.parsed_response
  end

  def threesixty_api( apitoken, access_token, service, method, params = {})
        url = "https://api.e.360.cn/2.0/#{service}/#{method}"
        response = HTTParty.post(url,
                                    timeout: 300,
                                    body: params,
                                headers: {
                                        'apiKey' => apitoken,
                                         'accessToken' => access_token,
                                        'serveToken' => Time.now.to_i.to_s
                                })
        @response = response
        if @response.headers["quotaremain"].to_i < 500
                then logger.info "|||||Not Enough Quota||||"
                return nil
        else
        return response.parsed_response
        end
  end

  def threesixty_campaignids(apitoken, access_token)
  	all_campaigns = []
  	campaign_array = []
  	result = threesixty_api(apitoken.to_s,access_token,"account", "getCampaignIdList")
    if result.nil?
      return nil
    end
    campaign_array = result["account_getCampaignIdList_response"]["campaignIdList"]["item"]
    if campaign_array.is_a?(Hash)
    	if !campaign_array.empty?
          all_campaigns << campaign_array
      end
    else
      all_campaigns = all_campaigns + campaign_array
    end
    if all_campaigns.count.to_i > 0
      return all_campaigns
  	else
      return nil
      end
  end


  def threesixty_campaignname(apitoken, access_token, campaignid)
	campaign_name = ""
	body = {}
	body[:idList] = "["+campaignid.to_s+"]"
	result = threesixty_api( apitoken.to_s, access_token, "campaign", "getInfoByIdList", body)
	if result.nil? then return nil end
	campaign_body= result["campaign_getInfoByIdList_response"]["campaignList"]["item"]
	campaign_name = campaign_body["name"]
	return campaign_name
  end

def threesixty_adgroupid(apitoken, access_token,campaignid)
  adgroup_id_arr = []
  adgroup_id_arr_str = ""
  body = {}
  body[:campaignId] = campaignid
  result = threesixty_api( apitoken.to_s, access_token, "group", "getIdListByCampaignId", body)
  if result.nil?
   return nil
  end
  adgroups = result["group_getIdListByCampaignId_response"]["groupIdList"]["item"]
  if adgroups.is_a?(Array)
    if adgroups.count.to_i > 0
      adgroups.each do |adgroups_d|
        adgroup_id_arr << adgroups_d.to_i
      end
    end
  else
    adgroup_id_arr << adgroups.to_i
  end
  if adgroup_id_arr.count.to_i > 0
	return adgroup_id_arr
	else
	return nil
  end
end

  def threesixty_adgroupname(apitoken, access_token, groupid)
	group_name = ""
	body = {}
	body[:idList] = "[" + groupid.to_s + "]"
	result = threesixty_api(apitoken.to_s, access_token, "group", "getInfoByIdList", body)
  	if result.nil? then return nil end
  	group_body= result["group_getInfoByIdList_response"]["groupList"]["item"]
  	group_name = group_body["name"]
  	return group_name
  end

  def threesixty_keywordid(apitoken, access_token,adgroupid)
  body = {}
  body[:groupId] = adgroupid.to_i
  @result = threesixty_api( apitoken.to_s, access_token, "keyword", "getIdListByGroupId", body)
  if @result.nil?
     return nil
  end
	all_keyword = []
  keyword_id_arr = []
  keyword_id_arr_str = ""
  if @result["keyword_getIdListByGroupId_response"]["failures"].nil?
    if !@result["keyword_getIdListByGroupId_response"]["keywordIdList"].nil?
      if !@result["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"].nil?
        @keyword_id_status_body = @result["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"]
          if @keyword_id_status_body.is_a?(Array)
            if @keyword_id_status_body.count.to_i > 0
            @keyword_id_status_body.each do |keyword_id_status_body_d|
            keyword_id_arr << keyword_id_status_body_d.to_i
              end
            end
          else
          keyword_id_arr << @keyword_id_status_body.to_i
          end
      end
    end
  end
  if keyword_id_arr.count.to_i > 0
    keyword_id_arr_str = keyword_id_arr.join(",")
    body = {}
    body[:idList] = "["+keyword_id_arr_str.to_s+"]"
    @result = threesixty_api( apitoken.to_s, access_token, "keyword", "getInfoByIdList", body)
    @kwarr= @result["keyword_getInfoByIdList_response"]["keywordList"]["item"]
    if @kwarr.is_a?(Hash)
      if !@kwarr.empty?
        all_keyword << @kwarr
      end
    else
      all_keyword = all_keyword + @kwarr
    end
	end
	if all_keyword.count.to_i > 0
  return all_keyword
	else
  return nil
  end
end

  def threesixty_campaignids(apitoken, access_token)
    all_campaigns = []
    campaign_array = []
    result = threesixty_api(apitoken.to_s,access_token,"account", "getCampaignIdList")
    if result.nil?
      return nil
    end
    campaign_array = result["account_getCampaignIdList_response"]["campaignIdList"]["item"]
    if campaign_array.is_a?(Hash)
      if !campaign_array.empty?
          all_campaigns << campaign_array
      end
    else
      all_campaigns = all_campaigns + campaign_array
    end
    if all_campaigns.count.to_i > 0
      return all_campaigns
    else
      return nil
      end
  end

end
