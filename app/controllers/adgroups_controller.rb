class AdgroupsController < ApplicationController
  before_action :find_adgroup, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :owned_adgroup, only: [:show, :edit, :update, :destroy]

  def retrieve_keywords
    @adgroup = Adgroup.find(params[:id])
    @username = @adgroup.campaign.account.username
    @password = @adgroup.campaign.account.password
    @apitoken = @adgroup.campaign.account.apitoken
    @apisecret = @adgroup.campaign.account.apisecret
    @target = "keywords"
    message = ::ApiCall::DataRetriever.new(reference_element: @adgroup, username: @username, password: @password, apitoken: @apitoken, apisecret: @apisecret, target: @target).call_api
    redirect_to @adgroup
    flash[:success] = message
  end


  def delete_keywords
    # operates under the assumption that user has already created an account.
    @adgroup = Adgroup.find(params[:id])

    if @adgroup.keywords.count > 0
      @adgroup.keywords.destroy_all
      flash[:success] = "Your keywords have been deleted."
    else
      flash[:success] = "No keywords to delete."

    end
    redirect_to @adgroup

  end

  # def index
  # end

  def show

  end

  def new
    @adgroup = Adgroup.new
  end

  def edit
  end

  def create
    @adgroup = Adgroup.new(adgroup_params)

    respond_to do |format|
      if @adgroup.save
        format.html { redirect_to @adgroup, notice: 'Adgroup was successfully created.' }
        format.json { render :show, status: :created, location: @adgroup }
      else
        format.html { render :new }
        format.json { render json: @adgroup.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @adgroup.update(adgroup_params)
        format.html { redirect_to @adgroup, notice: 'Adgroup was successfully updated.' }
        format.json { render :show, status: :ok, location: @adgroup }
      else
        format.html { render :edit }
        format.json { render json: @adgroup.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    session[:return_to] ||= request.referer
    @adgroup.destroy
    respond_to do |format|
      format.html {  redirect_to session.delete(:return_to), notice: 'Adgroup was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def find_adgroup
      @adgroup = Adgroup.find(params[:id])
    end

    def owned_adgroup
      unless current_user.id == @adgroup.campaign.account.user_id
      redirect_to root_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adgroup_params
      params.require(:adgroup).permit(:campaign_id, :name)
    end
end
