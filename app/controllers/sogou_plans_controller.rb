class SogouPlansController < ApplicationController
  before_action :find_sogou_plan, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :owned_sogou_plan, only: [:show, :edit, :update, :destroy]
  def new
    @sogou_plan = SogouPlan.new
  end

  def create
    @sogou_plan = SogouPlan.new(sogou_plan_params)
  end

  def destroy
    session[:return_to] ||= request.referer
    @sogou_plan.destroy
    respond_to do |format|
      format.html { redirect_to session.delete(:return_to), notice: 'Plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def sogou_retrieve_groups

    @plan = SogouPlan.find(params[:id])
    @account = @plan.account
    @username = @account.username
    @password = @account.password
    @apitoken = @account.apitoken

    @group_id_array = sogou_retrieve_group_ids(@username, @password, @apitoken)
    
    @group_id_array.each do |g|
      @account.group_id_array.push(g)
    end
    
    @user = sogou_api(@username,@password,@apitoken,"CpcGrpService")

    @update_status = @user.call(:get_cpc_grp_by_cpc_grp_id, message: {cpcGrpIds: @group_id_array})

    @update_status = @update_status.body.to_hash[:get_cpc_grp_by_cpc_grp_id_response][:cpc_grp_types]
   
    @update_status.each do |g|
      if g[:cpc_plan_id] == @plan.cpc_plan_id
        sg = SogouGroup.create(g)
        @plan.sogou_groups.push(sg)

      end
      
    end
    redirect_to @plan
    flash[:success] = "Grps for #{@account.channel}-#{@account.username} retrieved."

  end



  def sogou_delete_groups
    # operates under the assumption that user has already created an account.
    @sogou_plan = SogouPlan.find(params[:id])

    if @sogou_plan.sogou_groups.count > 0
      @sogou_plan.sogou_groups.destroy_all
      flash[:success] = "Your groups have been deleted."
    else
      flash[:success] = "No groups to delete."
    end
    redirect_to @sogou_plan

  end



  private 
    def find_sogou_plan
      @sogou_plan = SogouPlan.find(params[:id])
    end

    def owned_sogou_plan
      unless current_user.id == @sogou_plan.account.user_id
      redirect_to root_path
      end
    end
     def sogou_plan_params
      params.require(:sogou_plan).permit(:cpc_plan_id, :cpc_plan_name, :budget, :schedule,:pause,:join_union,:negative_words, :impressions, :clicks)
    end
end
