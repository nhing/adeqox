class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :super_admin_only
  before_action :set_user, only: [:destroy, :user_not_super_admin, :make_superadmin, ]
  before_action :user_not_super_admin, only: [:destroy, :make_superadmin]

  def index
    @users = User.all.order('created_at ASC')
  end

  def destroy
    @user.destroy!
    respond_to do |format|
      format.html { redirect_to users_path, notice: 'User account was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def make_superadmin
    @user.superadmin_role = true
    if @user.save
      respond_to do |format|
        format.html { redirect_to users_path, notice: 'User is now a superadmin.' }
        format.json { head :no_content }
      end
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
      if @user.save
        redirect_to users_path, notice: 'User was successfully created.'
      else
        render :new, notice: "Errors: #{@user.errors}"
    end
  end

 private

  def set_user
    @user = User.find(params[:id])
  end

  def super_admin_only
    unless current_user.superadmin_role?
    redirect_to root_path
    end
  end

  def user_not_super_admin
    if @user.superadmin_role?
      redirect_to users_path
    end
  end

  def user_params
    params.require(:user).permit(:email, :password, :superadmin_role)
  end
end
