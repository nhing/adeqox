class CampaignWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def threesixty_adgroupname(apitoken, access_token, groupid)
  group_name = ""
  body = {}
  body[:idList] = "[" + groupid.to_s + "]"
  result = threesixty_api(apitoken.to_s, access_token, "group", "getInfoByIdList", body)
    if result.nil?
      return
      nil
    end
    group_body= result["group_getInfoByIdList_response"]["groupList"]["item"]
    group_name = group_body["name"]
    return group_name
  end

  def threesixty_login(username,password,apitoken,apisecret)
                cipher_aes = OpenSSL::Cipher::AES.new(128, :CBC)
                cipher_aes.encrypt
                cipher_aes.key = apisecret[0,16]
                cipher_aes.iv = apisecret[16,16]
                encrypted = (cipher_aes.update(Digest::MD5.hexdigest(password)) + cipher_aes.final).unpack('H*').join
                url = "https://api.e.360.cn/account/clientLogin"
                response = HTTParty.post(url,
                :timeout => 300,
                :body => {
                        :username => username,
                        :passwd => encrypted[0,64]
                },
                :headers => {'apiKey' => apitoken }
                 )
                return response.parsed_response
  end

  def threesixty_api( apitoken, access_token, service, method, params = {})
        url = "https://api.e.360.cn/2.0/#{service}/#{method}"
        response = HTTParty.post(url,
                                    timeout: 300,
                                    body: params,
                                headers: {
                                        'apiKey' => apitoken,
                                         'accessToken' => access_token,
                                        'serveToken' => Time.now.to_i.to_s
                                })
        @response = response
        if @response.headers["quotaremain"].to_i < 500
                then logger.info "|||||Not Enough Quota||||"
                return nil
        else
        return response.parsed_response
        end
  end

  def threesixty_adgroupid(apitoken, access_token,campaignid)
  adgroup_id_arr = []
  adgroup_id_arr_str = ""
  body = {}
  body[:campaignId] = campaignid
  result = threesixty_api( apitoken.to_s, access_token, "group", "getIdListByCampaignId", body)
    if result.nil?
     return nil
    end
    adgroups = result["group_getIdListByCampaignId_response"]["groupIdList"]["item"]
    if adgroups.is_a?(Array)
      if adgroups.count.to_i > 0
        adgroups.each do |adgroups_d|
          adgroup_id_arr << adgroups_d.to_i
        end
      end
    else
      adgroup_id_arr << adgroups.to_i
    end
    if adgroup_id_arr.count.to_i > 0
    return adgroup_id_arr
    else
    return nil
    end
  end

  def perform(campaign_id, action)
    campaign = Campaign.find(campaign_id)

    if action == "retrieve_adgroups"
      username = campaign.account.username
      password = campaign.account.password
      apitoken = campaign.account.apitoken
      apisecret = campaign.account.apisecret
      response = threesixty_login(username, password, apitoken, apisecret)
      accesstoken = response["account_clientLogin_response"]["accessToken"]
        retrieved_adgroup_ids = threesixty_adgroupid(apitoken, accesstoken, campaign.channel_id)
        campaign_adgroups = campaign.adgroups

          if campaign_adgroups.count < 1
            retrieved_adgroup_ids.each do |id|
            name = threesixty_adgroupname(apitoken, accesstoken, id)
            adgroup = Adgroup.create(:channel_id => id, :campaign_id => campaign.id, :name => name)
            end
            message = "Your adgroups have been retrieved."

          else
            campaign_adgroup_ids = []
            campaign.adgroups.each do |a| campaign_adgroup_ids.push(a.channel_id)
            end
            if retrieved_adgroup_ids.sort == campaign_adgroup_ids.sort
              message = "Your adgroups are already up to date."
            else
            campaign_adgroup_ids.each do |id|
              unless id.in?retrieved_adgroup_ids
              Adgroup.where(campaign_id: campaign.id).where(channel_id: id).first.destroy
                end
              end
            retrieved_adgroup_ids.each do |id|
              unless id.in?campaign_adgroup_ids
                name = threesixty_adgroupname(apitoken, accesstoken, id)
                Adgroup.create(:channel_id => id, :campaign_id => campaign.id, :name => name)
                end
              end
            message = "Your adgroups have been updated."
            end
        end
      puts "Side worker retrieved  #{retrieved_adgroup_ids.count} adgroups "

    elsif action == "delete_all_adgroup"
      campaign.adgroups.destroy_all
      message = "Your Adgroups have been deleted. Refresh the page."
    end
  end
end
