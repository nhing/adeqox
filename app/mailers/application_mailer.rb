class ApplicationMailer < ActionMailer::Base
  default from: 'data@bmgww.com'
  layout 'mailer'
end
