class CreateCampaigns < ActiveRecord::Migration[5.2]
  def change
    create_table :campaigns do |t|
      t.string :name
      t.string :channel_id
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
