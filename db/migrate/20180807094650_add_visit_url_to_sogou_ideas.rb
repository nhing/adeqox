class AddVisitUrlToSogouIdeas < ActiveRecord::Migration[5.2]
  def change
    add_column :sogou_ideas, :visit_url, :string
  end
end
