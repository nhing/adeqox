class AddExactNegativeWordsToSogouPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :sogou_plans, :exact_negative_words, :text
  end
end
