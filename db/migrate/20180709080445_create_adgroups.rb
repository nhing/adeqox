class CreateAdgroups < ActiveRecord::Migration[5.2]
  def change
    create_table :adgroups do |t|
      t.string :name
      t.integer :channel_id, limit: 8
      t.references :campaign, foreign_key: true

      t.timestamps
    end
  end
end
