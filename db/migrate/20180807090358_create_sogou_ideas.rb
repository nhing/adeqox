class CreateSogouIdeas < ActiveRecord::Migration[5.2]
  def change
    create_table :sogou_ideas do |t|
      t.string :cpc_idea_id
      t.string :cpc_grp_id
      t.string :title
      t.string :description1
      t.string :description2
      t.string :visit_url
      t.string :show_url
      t.string :pause
      t.string :status
      t.text :opt

      t.string :sogou_group_id
      t.references :sogou_group, foreign_key: true
      t.timestamps
    end
  end
end
